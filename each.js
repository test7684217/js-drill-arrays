exports.each = (items, cb) => {
  try {
    if (!Array.isArray(items)) {
      console.log("Input is not an Array");
      return;
    }

    for (let index = 0; index < items.length; index++) {
      cb(items[index], index);
    }
  } catch (error) {
    console.log(error.message);
  }
};
