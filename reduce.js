exports.reduce = (items, cb, initialValue) => {
  try {
    if (items.length == 0) {
      return undefined;
    }

    let accumulator = initialValue;

    if (!accumulator && items.length > 0) {
      accumulator = items[0];
      for (let index = 1; index < items.length; index++) {
        accumulator = cb(accumulator, items[index]);
      }

      return accumulator;
    }

    for (let index = 0; index < items.length; index++) {
      accumulator = cb(accumulator, items[index]);
    }

    return accumulator;
    
  } catch (error) {
    console.log(error);
  }
};
