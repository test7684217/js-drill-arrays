exports.filter = (items, cb) => {
  try {
    if (!Array.isArray(items)) {
      console.log("Input is not an Array");
      return undefined;
    }

    let result = [];

    for (let index = 0; index < items.length; index++) {
      if (cb(items[index])) {
        result.push(items[index]);
      }
    }

    if (items.length != 0 && result.length == 0) {
      return undefined;
    }

    return result;
  } catch (error) {
    console.log(error.message);
  }
};
