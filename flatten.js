exports.flatten = (items) => {
  try {
    if (!Array.isArray(items)) {
      console.log("Input is not an Array");
      return undefined;
    }
    let result = [];
    
    solve(items, result);
    
    return result;

  } catch (error) {
    console.log(error);
  }
};

function solve(items, result) {
  for (let index = 0; index < items.length; index++) {
    if (Array.isArray(items[index])) {
      solve(items[index], result);
    } else {
      result.push(items[index]);
    }
  }
}
