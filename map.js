exports.map = (items, cb) => {
  try {
    if (!Array.isArray(items)) {
      console.log("Input is not an Array");
      return undefined;
    }
    let result = [];

    for (let index = 0; index < items.length; index++) {
      const x = cb(index, items[index]);
      result.push(x);
    }

    return result;
  } catch (error) {
    console.log(error);
  }
};
