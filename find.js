exports.find = (items, cb) => {
  try {
    if (!Array.isArray(items)) {
      console.log("Input is not an Array ");
      return undefined;
    }

    for (let index = 0; index < items.length; index++) {
      if (cb(items[index])) {
        return items[index];
      }
    }

  } catch (error) {
    console.log(error);
  }

  return undefined;
};
